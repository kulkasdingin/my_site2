from django.shortcuts import render
from .forms import  SnippetForm

def index(request):
    return render(request, 'index.html', {})

def daftar(request):
    return render(request, 'daftar.html', {})

def setelahdaftar(request):
    return render(request, 'setelahdaftar.html', {})

# def contact(request):
#     if request.method == 'POST':
#         form = ContactForm(request.POST)
#         if form.is_valid():
#             name = form.cleaned_data['name']
#             email = form.cleaned_data['email']
#
#
#
#     form = ContactForm()
#
#     return render(request, 'form.html', {'form':form})

def snippet_detail(request):
    if request.method == 'POST':
        form = SnippetForm(request.POST)
        if form.is_valid():
            form.save()

    form = SnippetForm()
    return render(request, 'form.html', {'form':form})


# Create your views here.
