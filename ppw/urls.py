from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('index/', views.index, name='index'),
    path('daftar/', views.daftar, name='daftar'),
    path('setelahdaftar', views.setelahdaftar, name='setelahdaftar'),
    # path('contact',views.contact,name='contact'),
    path('snippet',views.snippet_detail,name='snippet')
]