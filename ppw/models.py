from django.db import models

class Snippet(models.Model):

    KATEGORI = (
        ('tugas','Tugas'),
        ('main', 'Main'),
        ('organisasi', 'Organisasi'),
    )

    hari = models.CharField(max_length=10)
    # tanggal = models.DateTimeField()
    nama_kegiatan = models.CharField(max_length=20)
    tempat = models.CharField(max_length=20)
    kategori = models.CharField(max_length=1, choices=KATEGORI)

    def __str__(self):
        return self.hari

# Create your models here.
